import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: Post[];

  constructor(public navCtrl: NavController, private dataProvider: DataProvider) {
    this.dataProvider.getPosts().subscribe((posts) => {
      this.posts = posts;
    })
  }

}
interface Post {
  id: number,
  title: string,
  body: string,
  userId: number
}