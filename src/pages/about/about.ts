import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  name: string;
  constructor(public navCtrl: NavController) {
    this.name = 'Form';
  }
  onClick() {
    if(this.name == 'Test'){
      this.name = 'Form';
    }
    else{
      this.name = 'Test';
    }
  }
}
